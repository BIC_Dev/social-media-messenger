package models

import (
	"net/http"
	"time"

	"gitlab.com/BIC_Dev/social-media-messenger/utils"
	"gitlab.com/BIC_Dev/social-media-messenger/utils/db"
)

// Twitter table structure
type Twitter struct {
	ID                      uint   `gorm:"primary_key;AUTO_INCREMENT"`
	TwitterID               string `gorm:"type:varchar(100)"`
	DiscordMessageID        string `gorm:"type:varchar(100);index"`
	DiscordGuildID          string `gorm:"varchar(100)"`
	DiscordGuildName        string `gorm:"varchar(250)"`
	DiscordChannelID        string `gorm:"varchar(100)"`
	DiscordChannelName      string `gorm:"varchar(250)"`
	DiscordAuthorID         string `gorm:"type:varchar(250)"`
	DiscordAuthorName       string `gorm:"type:varchar(250)"`
	DiscordMediaURL         string `gorm:"type:varchar(250)"`
	DiscordMediaDescription string `gorm:"type:varchar(250)"`
	CreatedAt               time.Time
	UpdatedAt               time.Time
	DeletedAt               *time.Time
}

// TableName Set User's table name to be `profiles`
func (Twitter) TableName() string {
	return "twitter"
}

// Create adds a record to DB
func (t *Twitter) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create twitter post in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetAll gets all tweet from the DB by discord_id
func (t *Twitter) GetAll(DBStruct db.Interface) ([]*Twitter, *utils.ModelError) {
	var tweets []*Twitter
	result := DBStruct.GetDB().Where("discord_message_id = ?", t.DiscordMessageID).First(&tweets)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetStatus(http.StatusNotFound)
		modelError.SetMessage("Unable to find tweet in DB")
		return nil, modelError
	}

	return tweets, nil
}

// Update updates a tweet in the DB
func (t *Twitter) Update(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Model(&Twitter{}).Where("discord_message_id = ?", t.DiscordMessageID).Update(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetStatus(http.StatusNotFound)
		modelError.SetMessage("Unable to update tweet in DB")
		return modelError
	}

	return nil
}

// Delete soft deletes a tweet by ID in DB
func (t *Twitter) Delete(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Where("id = ?", t.ID).Delete(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetStatus(http.StatusNotFound)
		modelError.SetMessage("Unable to delete tweet in DB")
		return modelError
	}

	return nil
}
