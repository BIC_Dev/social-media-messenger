package models

import (
	"net/http"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/social-media-messenger/utils"
)

// Facebook table structure
type Facebook struct {
	ID                      uint   `gorm:"primary_key;AUTO_INCREMENT"`
	FacebookID              string `gorm:"type:varchar(100)"`
	DiscordMessageID        string `gorm:"type:varchar(100);index"`
	DiscordGuildID          string `gorm:"varchar(100)"`
	DiscordGuildName        string `gorm:"varchar(250)"`
	DiscordChannelID        string `gorm:"varchar(100)"`
	DiscordChannelName      string `gorm:"varchar(250)"`
	DiscordAuthorID         string `gorm:"type:varchar(250)"`
	DiscordAuthorName       string `gorm:"type:varchar(250)"`
	DiscordMediaURL         string `gorm:"type:varchar(250)"`
	DiscordMediaDescription string `gorm:"type:varchar(250)"`
	CreatedAt               time.Time
	UpdatedAt               time.Time
	DeletedAt               *time.Time
}

// TableName Set User's table name to be `profiles`
func (Facebook) TableName() string {
	return "facebook"
}

// Create adds a record to DB
func (f *Facebook) Create(DB *gorm.DB) *utils.ModelError {
	result := DB.Create(&f)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create Facebook post in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// Get gets a facebook post from the DB
func (f *Facebook) Get(DB *gorm.DB) *utils.ModelError {
	result := DB.Where("discord_message_id = ?", f.DiscordMessageID).First(&f)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetStatus(http.StatusNotFound)
		modelError.SetMessage("Unable to find facebook post in DB")
		return modelError
	}

	return nil
}

// Update updates a facebook post in the DB
func (f *Facebook) Update(DB *gorm.DB) *utils.ModelError {
	result := DB.Model(&Facebook{}).Where("discord_message_id = ?", f.DiscordMessageID).Update(&f)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetStatus(http.StatusNotFound)
		modelError.SetMessage("Unable to update facebook post in DB")
		return modelError
	}

	return nil
}

// Delete soft deletes a facebook post in the DB
func (f *Facebook) Delete(DB *gorm.DB) *utils.ModelError {
	result := DB.Delete(&f)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetStatus(http.StatusNotFound)
		modelError.SetMessage("Unable to delete facebook post in DB")
		return modelError
	}

	return nil
}
