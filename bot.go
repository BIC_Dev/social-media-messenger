package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/ChimeraCoder/anaconda"
	"github.com/bwmarrin/discordgo"
	fb "github.com/huandu/facebook"
	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/social-media-messenger/models"
	"gitlab.com/BIC_Dev/social-media-messenger/services"
	"gitlab.com/BIC_Dev/social-media-messenger/utils"
	"gitlab.com/BIC_Dev/social-media-messenger/utils/db"
)

// Post struct
type Post struct {
	DiscordGuild     *discordgo.Guild
	DiscordChannel   *discordgo.Channel
	DiscordMessageID string
	MessageAuthorID  string
	MessageAuthor    string
	Description      string
	MediaURLs        []string
}

// Config data for the bot
var Config *utils.Config

// Log struct for logging
var Log *utils.Log

func main() {
	env := os.Getenv("ENVIRONMENT")
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))

	if err != nil {
		log.Fatal(err.Error())
	}

	Log = utils.InitLog(logLevel)

	discordToken := os.Getenv("DISCORD_TOKEN")

	Config = utils.GetConfig(env)

	if os.Getenv("MIGRATE") == "TRUE" {
		migrationErrors := migrateTables(models.Twitter{}, models.Facebook{})

		if migrationErrors != nil {
			Log.Log(migrationErrors, Log.LogFatal)
		}
	}

	dg, discErr := discordgo.New("Bot " + discordToken)

	if discErr != nil {
		Log.Log(fmt.Sprintf("Error creating Discord session: %s", discErr.Error()), Log.LogFatal)
		return
	}

	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	// Register the messageDelete func as a callback for MessageDelete events
	dg.AddHandler(messageDelete)

	// Open a websocket connection to Discord and begin listening.
	openErr := dg.Open()
	if openErr != nil {
		Log.Log(fmt.Sprintf("Error opening connection: %s", openErr.Error()), Log.LogFatal)
		return
	}

	// Wait here until CTRL-C or other term signal is received.
	Log.Log("Bot is now running.  Press CTRL-C to exit.", Log.LogInformation)
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if !isWhitelistedChannel(m.ChannelID) {
		Log.Log(fmt.Sprintf("MessageCreate event sent from non-whitelisted channel: %s", m.ChannelID), Log.LogInformation)
		return
	}

	if m.Author.ID == s.State.User.ID {
		Log.Log("MessageCreate event sent by Social Media Messaging Bot", Log.LogInformation)
		return
	}

	if len(m.Attachments) <= 0 {
		Log.Log("MessageCreate event sent without attachement", Log.LogInformation)
		return
	}

	id := m.ID
	message := m.Content
	authorID := m.Author.ID
	author := m.Author.Username

	Log.Log(fmt.Sprintf("MessageCreate event for Message ID: %s", id), Log.LogInformation)

	DBStruct, getDBErr := getDB()

	if getDBErr != nil {
		Log.Log("Ending MessageCreate due to no DB", Log.LogHigh)
		return
	}

	defer DBStruct.GetDB().Close()

	var imageUrls []string
	maxImageSizeBytes := 5000000

	for _, v := range m.Attachments {
		if v.Size > maxImageSizeBytes {
			Log.Log("MessageCreate event sent with image > max size allowed", Log.LogInformation)
			continue
		}

		if !validImageType(v.Filename) {
			Log.Log("MessageCreate event sent with invalid image type", Log.LogInformation)
			continue
		}

		imageUrls = append(imageUrls, v.URL)
	}

	if len(imageUrls) <= 0 {
		Log.Log("MessageCreate event sent without valid attachment", Log.LogInformation)
		return
	}

	guild, guildErr := s.Guild(m.GuildID)

	if guildErr != nil {
		Log.Log(fmt.Sprintf("Error getting guild for ID (%s): %s", m.GuildID, guildErr.Error()), Log.LogHigh)
	}

	channel, channelErr := s.Channel(m.ChannelID)

	if channelErr != nil {
		Log.Log(fmt.Sprintf("Error getting channel for ID (%s): %s", m.ChannelID, channelErr.Error()), Log.LogHigh)
	}

	post := Post{
		DiscordGuild:     guild,
		DiscordChannel:   channel,
		DiscordMessageID: id,
		MessageAuthorID:  authorID,
		MessageAuthor:    author,
		Description:      message,
		MediaURLs:        imageUrls,
	}

	_, twitterPostErrors := createTwitterPost(&post, &DBStruct)

	if len(twitterPostErrors) > 0 {
		for _, err := range twitterPostErrors {
			Log.Log(fmt.Sprintf("Failed to create twitter post with error: %s", err.Error()), Log.LogInformation)
		}
	}

	twitterEmojiID := Config.Twitter.EmojiDefault

	if guild != nil {
		for _, emoji := range guild.Emojis {
			if emoji.Name == Config.Twitter.EmojiCustom {
				twitterEmojiID = emoji.APIName()
			}
		}
	}

	reactionError := s.MessageReactionAdd(m.ChannelID, m.Message.ID, twitterEmojiID)

	if reactionError != nil {
		Log.Log(reactionError.Error(), Log.LogHigh)
	}

	// Write the image and message to Facebook
	// Write Discord message ID and Facebook Post ID to DB
	/*
		_, facebookPostErrors := createFacebookPost(&post, DB)

		if len(facebookPostErrors) > 0 {
			for _, err := range facebookPostErrors {
				fmt.Printf("Failed to create facebook post with error: %s", err.Error())
			}
		}
	*/
}

func messageDelete(s *discordgo.Session, m *discordgo.MessageDelete) {
	if !isWhitelistedChannel(m.ChannelID) {
		Log.Log(fmt.Sprintf("MessageDelete event sent from non-whitelisted channel: %s", m.ChannelID), Log.LogInformation)
		return
	}

	Log.Log(fmt.Sprintf("MessageDelete event for Message ID: %s", m.ID), Log.LogInformation)

	DBStruct, getDBErr := getDB()

	if getDBErr != nil {
		Log.Log("Ending MessageDelete due to no DB", Log.LogHigh)
		return
	}

	defer DBStruct.GetDB().Close()

	post := Post{
		DiscordMessageID: m.ID,
	}

	deleteTwitterErrors := deleteTwitterPost(&post, &DBStruct)

	if deleteTwitterErrors != nil {
		for _, err := range deleteTwitterErrors {
			Log.Log(fmt.Sprintf("Failed to delete twitter post with error: %s", err.Error()), Log.LogMedium)
		}
	}

	// Look up Discord message ID in DB from Facebook table
	// Delete image from Facebook by Facebook Post ID
}

func getTwitterClient() *anaconda.TwitterApi {
	twitterConsumerKey := os.Getenv("TWITTER_CONSUMER_API_KEY")
	twitterConsumerKeySecret := os.Getenv("TWITTER_CONSUMER_API_KEY_SECRET")
	twitterAccessToken := os.Getenv("TWITTER_ACCESS_TOKEN")
	twitterAccessTokenSecret := os.Getenv("TWITTER_ACCESS_TOKEN_SECRET")

	return anaconda.NewTwitterApiWithCredentials(twitterAccessToken, twitterAccessTokenSecret, twitterConsumerKey, twitterConsumerKeySecret)
}

func getDB() (db.Interface, *utils.ModelError) {
	var DBStruct db.Interface
	var err *utils.ModelError

	switch Config.DB.Type {
	case "SQLite3":
		Log.Log("Connecting to SQLite3 DB", Log.LogInformation)
		SQLite3 := db.SQLite3{}
		err = SQLite3.Connect(Config)

		if err != nil {
			Log.Log(fmt.Sprintf("%s: %s", err.GetMessage(), err.Error()), Log.LogHigh)
		}

		DBStruct = &SQLite3
	case "PostgreSQL":
		Log.Log("Connecting to PostgreSQL", Log.LogInformation)
		PostgreSQL := db.PostgreSQL{
			Username: os.Getenv("POSTGRES_USERNAME"),
			Password: os.Getenv("POSTGRES_PASSWORD"),
		}

		err = PostgreSQL.Connect(Config)

		if err != nil {
			Log.Log(fmt.Sprintf("%s: %s", err.GetMessage(), err.Error()), Log.LogHigh)
		}

		DBStruct = &PostgreSQL
	default:
		Log.Log("Invalid DB type", Log.LogFatal)
	}

	if err != nil {
		return nil, err
	}

	Log.Log("Succesfully connected to DB", Log.LogInformation)

	return DBStruct, nil
}

func createTwitterPost(p *Post, DBStruct *db.Interface) ([]*anaconda.Tweet, []error) {
	twitterAPI := getTwitterClient()
	twitter := services.Twitter{
		Client: twitterAPI,
	}

	var tweets []*anaconda.Tweet
	var errors []error

	for _, url := range p.MediaURLs {
		twitterPost := services.TwitterPost{
			MediaURL:         url,
			MediaDescription: p.Description,
			MediaAuthor:      p.MessageAuthor,
		}

		aTweet, err := twitter.CreatePost(&twitterPost)

		if err != nil {
			errors = append(errors, err)
			continue
		}

		Log.Log("Twitter message successfully posted", Log.LogInformation)

		tweets = append(tweets, aTweet)

		var guildID string
		var guildName string
		var channelID string
		var channelName string

		if p.DiscordGuild != nil {
			guildID = p.DiscordGuild.ID
			guildName = p.DiscordGuild.Name
		}

		if p.DiscordChannel != nil {
			channelID = p.DiscordChannel.ID
			channelName = p.DiscordChannel.Name
		}

		twitterDB := models.Twitter{
			TwitterID:               aTweet.IdStr,
			DiscordMessageID:        p.DiscordMessageID,
			DiscordGuildID:          guildID,
			DiscordGuildName:        guildName,
			DiscordChannelID:        channelID,
			DiscordChannelName:      channelName,
			DiscordAuthorID:         p.MessageAuthorID,
			DiscordAuthorName:       p.MessageAuthor,
			DiscordMediaURL:         twitterPost.MediaURL,
			DiscordMediaDescription: p.Description,
		}

		twitterDBErr := twitterDB.Create(*DBStruct)

		if twitterDBErr != nil {
			errors = append(errors, twitterDBErr)
		}

		Log.Log("Twitter post successfully added to DB", Log.LogInformation)
	}

	return tweets, errors
}

func deleteTwitterPost(p *Post, DBStruct *db.Interface) []error {
	twitterDB := models.Twitter{
		DiscordMessageID: p.DiscordMessageID,
	}

	tweets, twitterDBGetErr := twitterDB.GetAll(*DBStruct)

	var deleteErrors []error

	if twitterDBGetErr != nil {
		deleteErrors = append(deleteErrors, twitterDBGetErr)
		return deleteErrors
	}

	if len(tweets) == 0 {
		Log.Log("No Twitter post found in database to delete", Log.LogInformation)
		return nil
	}

	twitterAPI := getTwitterClient()
	twitter := services.Twitter{
		Client: twitterAPI,
	}

	for _, aTweet := range tweets {
		twitterID, convErr := strconv.Atoi(aTweet.TwitterID)

		if convErr != nil {
			deleteErrors = append(deleteErrors, convErr)
		}

		_, deleteErr := twitter.DeletePost(int64(twitterID))

		if deleteErr != nil {
			deleteErrors = append(deleteErrors, deleteErr)
			continue
		}

		Log.Log("Twitter message successfully deleted", Log.LogInformation)

		deleteDBErr := aTweet.Delete(*DBStruct)

		if deleteDBErr != nil {
			deleteErrors = append(deleteErrors, deleteDBErr)
			continue
		}

		Log.Log("Twitter post successfully deleted from DB", Log.LogInformation)
	}

	return nil
}

func createFacebookPost(p *Post, DB *gorm.DB) ([]*fb.Result, []error) {
	facebook := services.Facebook{
		AuthToken: os.Getenv("FACEBOOK_APP_ACCESS_TOKEN"),
		PageID:    os.Getenv("FACEBOOK_PAGE_ID"),
	}

	var posts []*fb.Result
	var errors []error

	for _, url := range p.MediaURLs {
		facebookPost := services.FacebookPost{
			MediaURL:         url,
			MediaDescription: p.Description,
			MediaAuthor:      p.MessageAuthor,
		}

		fbPost, createPostErr := facebook.CreatePost(&facebookPost)

		if createPostErr != nil {
			errors = append(errors, createPostErr)
			continue
		}

		posts = append(posts, fbPost)
	}

	return posts, errors
}

func addReaction(m *discordgo.MessageCreate, reactionType string) {

}

// validImageType checks if the image sent in discord is a valid format for Twitter
func validImageType(imageName string) bool {
	validTypes := []string{
		".jpg",
		".jpeg",
		".png",
	}

	for _, v := range validTypes {
		if strings.Contains(imageName, v) {
			return true
		}
	}

	return false
}

func isWhitelistedChannel(channelID string) bool {
	for _, ID := range Config.Discord.ChannelWhitelist {
		if ID == channelID {
			return true
		}
	}

	return false
}

func migrateTables(tables ...interface{}) []*utils.ModelError {
	Log.Log("Migrating tables: twitter, facebook", Log.LogInformation)
	var migrationErrors []*utils.ModelError

	DBStruct, getDBErr := getDB()

	if getDBErr != nil {
		Log.Log("Ending MigrateTables due to no DB", Log.LogHigh)
		return append(migrationErrors, getDBErr)
	}

	defer DBStruct.GetDB().Close()

	for _, table := range tables {
		migrationError := DBStruct.Migrate(table)

		if migrationError != nil {
			migrationErrors = append(migrationErrors, migrationError)
		}
	}

	if len(migrationErrors) > 0 {
		return migrationErrors
	}

	return nil
}
