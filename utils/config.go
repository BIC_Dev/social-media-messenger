package utils

import (
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// Config struct that contians the structure of the config
type Config struct {
	DB struct {
		Type string `yaml:"type"`
	} `yaml:"DB"`
	SQLite3 struct {
		Path   string `yaml:"path"`
		DBName string `yaml:"db_name"`
	} `yaml:"SQLITE3"`
	PostgreSQL struct {
		Host   string `yaml:"host"`
		Port   string `yaml:"port"`
		DBName string `yaml:"db_name"`
	} `yaml:"POSTGRESQL"`
	Twitter struct {
		EmojiDefault string `yaml:"emoji_default"`
		EmojiCustom  string `yaml:"emoji_custom"`
	} `yaml:"TWITTER"`
	Facebook struct {
		EmojiDefault string `yaml:"emoji_default"`
		EmojiCustom  string `yaml:"emoji_custom"`
	} `yaml:"FACEBOOK"`
	Discord struct {
		ChannelWhitelist []string `yaml:"channel_whitelist"`
	} `yaml:"DISCORD"`
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(env string) *Config {
	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		log.Fatal("Missing config file at path: " + configFile)
	}

	defer f.Close()

	var config Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&config)

	if err != nil {
		log.Fatal("Could not parse config file")
	}

	return &config
}
